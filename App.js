import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect }from 'react';
import { StyleSheet, Text, View, SafeAreaView, 
        TextInput, TouchableOpacity, FlatList } from 'react-native';

import Message from './app/components/Message';
import { Feather } from '@expo/vector-icons'; 

export default function App() {
  
  const [inputTextHeight, setInputTextHeight] = useState(30);
  const [inputTextContent, setInputTextContent] = useState('');
  const [chatMessages, setChatMessages] = useState([]);

  const [clientId, setClientId] = useState(0);

  let ws = React.useRef(new WebSocket('ws://localhost:8080')).current;
  
  useEffect(() => {
    ws.onopen = () => {
      console.log('Connected to the server');
    };

    ws.onclose = (e) => {
      console.log('Disconnected. Check internet or server.');
    };

    ws.onerror = (e) => {
      console.log(e.message);
    };

    ws.onmessage = (e) => {
      let msg = JSON.parse(e.data);
      console.log(msg);
      switch(msg.type) {
          case "id":
              setClientId(msg.clientId);
              break;
          case "msg":
              console.log('try to add to chat: ' + msg.text);
              setChatMessages(prevArray => [...prevArray, {
                id: msg.id,
                clientId: msg.clientId,
                text: msg.text,
              }]);
              console.log(chatMessages.length);
              break;
        
      }
    };
  }, []);

  const submitChatMessage = () => {
    let msg = {
      type: "msg",
      clientId: clientId,
      text: inputTextContent,
    }
    ws.send(JSON.stringify(msg));

    setInputTextContent('');
  }

  return (
    <View style={styles.container}>
      <View style={styles.mainContainer}>
        <Text>Main</Text>
      </View>
      <View style={styles.chatContainer}>
        <View style={styles.chatSection}>
          <FlatList
            data={chatMessages}
            renderItem={({item}) => (
              <Message {...{item, clientId}}></Message>
            )}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={styles.inputSection}>
          <View style={styles.textInputContainer}>
            <TextInput  style={[styles.textInput, {height: Math.min(140, inputTextHeight)}]}
                        placeholder={"Your Message ..."}
                        multiline={true}
                        onChange={(event) => {
                          setInputTextContent(event.nativeEvent.text);
                        }}
                        onContentSizeChange={(event) => {
                          setInputTextHeight(event.nativeEvent.contentSize.height);
                        }}
                        value={inputTextContent}
                        />
          </View>
          <TouchableOpacity
            style={styles.sendButton}
            onPress={() => submitChatMessage()}
          >
            <Feather name="send" size={24} 
                style={styles.sendButtonText} />
          </TouchableOpacity>
        </View>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

//https://github.com/Savinvadim1312/WhatsappClone

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'black',
  },
  chatContainer: {
    width: 500,
    backgroundColor: '#161617',
    borderLeftColor: 'powderblue',
    borderLeftWidth: 2,
    padding: 10,
  },
  chatSection: {
    flex: 1,
  },
  inputSection: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'flex-end',
  },
  textInputContainer: {
    flexDirection: 'row',
    backgroundColor: 'powderblue',
    padding: 15,
    borderRadius: 50,
    marginRight: 10,
    flex: 1,
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    marginHorizontal: 10,
    outlineStyle: 'none',
    //let the input shrink after deleting lines
    borderWidth: 0.5, // any value is required
    borderColor: 'transparent',
    overflow: 'hidden',
  },
  sendButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1c56ba',
    width: 50,
    height: 50,
    borderRadius: 15,
  },
  sendButtonText: {
    alignItems: "center",
    padding: 10,
    color: 'white',
  }
});
