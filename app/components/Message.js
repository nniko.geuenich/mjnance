
import React from 'react';
import { StyleSheet, Text, View} from 'react-native';


function Message({item, clientId}) {

    const isMe = item.clientId === clientId;

    return (
        <View style={[styles.msgContainer, {
            marginLeft: isMe ? 'auto' : 10,
            marginRight: isMe ? 10 : 'auto',
            backgroundColor: isMe ? '#bdd5ff' : '#fff',
        }]}>
            {!isMe &&
            <Text style={styles.authorText}>
                {item.clientId}
            </Text>
            }
            <Text style={styles.msgText}>
                {item.text}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    msgContainer: {
        flexDirection: 'column',
        width: 'fit-content',
        borderRadius: 20,
        padding: 10,
        marginTop: 10,
        maxWidth: '75%',
    },
    authorText: {
        color: '#1c56ba',
        paddingBottom: 5,
    },
    msgText: {
        fontSize: 17,
        fontFamily: 'Roboto',
    },
});

export default Message;

